Composition
===========

A wandering simulator I made for [Eevee's GAMES MADE QUICK??? jam](https://itch.io/jam/games-made-quick). Play it over here: https://chz.itch.io/composition

Music loops made with [Terry Cavanagh's Bosca Ceoil](https://terrycavanagh.itch.io/bosca-ceoil). The source file with all the patterns is in the root directory.

This code is released under MIT (see LICENSE.txt).
