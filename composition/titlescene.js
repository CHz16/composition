// titlescene.js
// Controller for the game scene.
function TitleSceneController() {
    this.chaos = 0;
    this.transitioning = true;
}

TitleSceneController.prototype.draw = function(timestamp) {
    // If we're not transitioning, handle player input.
    if (!this.transitioning) {        
        // Update the chaos.
        var xVelocity = 0, yVelocity = 0;
        if (upKeyDown()) {
            // up
            yVelocity -= 1;
        }
        if (downKeyDown()) {
            // down
            yVelocity += 1;
        }
        if (leftKeyDown()) {
            // left
            xVelocity -= 1;
        } 
        if (rightKeyDown()) {
            // right
            xVelocity += 1;
        }
        if (xVelocity != 0 && yVelocity != 0) {
            xVelocity /= 2;
            yVelocity /= 2;
        }
        this.chaos += (xVelocity + yVelocity) * openingAndEndingChaosStep;
    
        if (this.chaos < openingAndEndingChaosStep) {
            this.chaos = openingAndEndingChaosStep;
        } else if (this.chaos > 1) {
            this.chaos = 1;
        }
        setMusicChaos(this.chaos);
    }
    
    drawTextCenteredAt("Composition", gameCanvasContext, gameWidth / 2, 50, this.chaos);
    drawTextCenteredAt("made by CHz for GAMES MADE QUICK???", gameCanvasContext, gameWidth / 2, 50 + lineHeight * 2, this.chaos);
    drawTextCenteredAt("Press [Z], [X], or [SPACE] to start", statusCanvasContext, gameWidth / 2, 50, this.chaos);
}

TitleSceneController.prototype.startTransitionIn = function() {
    this.chaos = openingAndEndingChaosStep;
    this.transitioning = true;
}

TitleSceneController.prototype.finishTransitionIn = function() {
    this.transitioning = false;
}

TitleSceneController.prototype.startTransitionOut = function() {
    this.transitioning = true;
}

TitleSceneController.prototype.finishTransitionOut = function() {
    
}

TitleSceneController.prototype.triggerAction = function() {
    if (!this.transitioning) {
        startTransition(1);
    }
}
