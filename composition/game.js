// game.js
// Composition main game code.


//
// GAME PARAMETERS
//

var gameWidth = 800;
var gameHeight = 400;
var statusHeight = 200;
var tileSize = 40;
var numberOfBackgroundLoops = 20;
var sceneTransitionDuration = 30;

var playerSpeed = 3;
var chaosMaxUncertainty = 20;
var possibleScrambleCharacters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

// Arrows = 38, 40, 37, 39
// WASD = 87, 83, 65, 68
// IJKL = 73, 75, 74, 76
// ZX[space] = 90, 88, 32
// escape = 27
var eatenKeyCodes = [38, 40, 37, 39, 87, 83, 65, 68, 73, 75, 74, 76, 90, 88, 32, 27];
var keyDelay = 200; // milliseconds

var fontFamily = "monospace";
var fontSize = 20;
var font = fontSize + "px " + fontFamily;
var lineHeight = fontSize * 1.4;
var characterWidth; // calculated in initialization

var statusTextStartX = 20, statusTextStartY = 30, statusTextWidth = 760;
var inventoryCenterLine = 600;

var chaosWaveHoldDuration = 60, chaosWaveHoldTransitionDuration = 60; // frames
var chaosWaveMaxChaos = 0.2;
var chaosWaveCycleLength = 10, chaosWaveMaxPerturbation = 10;

var openingAndEndingChaosStep = 0.005;
var endingTextStartX = 20, endingTextStartY = 30, endingTextWidth = gameWidth - 2 * endingTextStartX;


//
// GLOBALS
//

var audioContext;
var musicEngine;
var gameCanvas, gameCanvasContext;
var statusCanvas, statusCanvasContext;
var decomposedBitmaps = new Object();

var scenes = [], currentScene = 0; // 0 = title, 1 = game, 2 = ending
var sceneTransitionTicks, sceneTransitionDestination = -1, sceneTransitionDirection;
var musicChaos = 0;

var raf;
var keyDown = [];
var lastActionKeyPressed = "[Z]";


//
// INITIALIZATION
//


window.onload = init;
function init() {
    let gameCanvasThings = makeScaledCanvas(gameWidth, gameHeight);
    gameCanvas = gameCanvasThings.canvas;
    gameCanvasContext = gameCanvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.body.replaceChild(gameCanvas, document.getElementById("gamecanvas"));
    gameCanvas.style.width = gameWidth + "px";
    gameCanvas.style.height = gameHeight + "px";

    let statusCanvasThings = makeScaledCanvas(gameWidth, statusHeight);
    statusCanvas = statusCanvasThings.canvas;
    statusCanvasContext = statusCanvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.body.replaceChild(statusCanvas, document.getElementById("statuscanvas"));
    statusCanvas.style.width = gameWidth + "px";
    statusCanvas.style.height = statusHeight + "px";


    var clickToPlay = function(evt) {
        gameCanvas.onmouseup = undefined;
        statusCanvas.onmouseup = undefined;
        init2();
    }

    gameCanvas.onmouseup = clickToPlay;
    statusCanvas.onmouseup = clickToPlay;
    drawLoadingMessage("click to play");
}

function init2() {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    audioContext.resume(); // Safari 12 autoplay policy pls

    var sounds = [];
    for (var i = 1; i <= numberOfBackgroundLoops; i++) {
        sounds.push({key: i, urls: [i + ".ogg", i + ".mp3"]});
    }
    var base = "audio/loops/";

    var backgroundLoopBuffers = [];
    var loadCallback = function(preloader, soundsHandled, index) {
        backgroundLoopBuffers.push(preloader.soundBuffers[sounds[index].key]);
        drawLoadingMessage("loaded music " + soundsHandled + "/" + preloader.sounds.length);

        if (soundsHandled == preloader.sounds.length) {
            init3(backgroundLoopBuffers);
        }
    };
    var errorCallback = function(preloader, soundsHandled, index) {
        drawLoadingMessage("whoops, couldn't load music loop " + (index + 1));
        return true;
    };

    var preloader = new SoundPreloader(sounds, audioContext, {base: base, loadCallback: loadCallback, errorCallback: errorCallback});
    drawLoadingMessage("requesting music");
}

function init3(backgroundLoopBuffers) {
    musicEngine = new LoopEngine(backgroundLoopBuffers, audioContext, {chaos: 0});

    var bitmapNames = ["heart", "radio", "fence", "fencecorner", "fencet", "fence4way", "bush", "house", "garage", "car", "forsalesign", "tree", "pool", "gate", "wall", "wallcap", "wallcorner", "wallcornerwithfence", "mansion", "clippedbush1", "clippedbush2", "tombstone", "church", "bench", "swings", "tenniscourt", "middleschool", "elementaryschool", "musicstore", "radiostore", "burgerjoint", "knittingstore", "coffeeshop", "toystore", "bank", "realtor", "carlot"];
    var loadCallback = function(preloader, imagesHandled, index) {
        drawLoadingMessage("loaded pic " + imagesHandled + "/" + preloader.keys.length);

        // Decompose the RGB components of the image into 3 separate canvases.
        var image = preloader.getImage(bitmapNames[index]);

        var redCanvas = document.createElement("canvas");
        var redCanvasContext = redCanvas.getContext("2d");
        var redImageData = new ImageData(image.width, image.height);
        redCanvas.width = image.width;
        redCanvas.height = image.height;
        var greenCanvas = document.createElement("canvas");
        var greenCanvasContext = greenCanvas.getContext("2d");
        var greenImageData = new ImageData(image.width, image.height);
        greenCanvas.width = image.width;
        greenCanvas.height = image.height;
        var blueCanvas = document.createElement("canvas");
        var blueCanvasContext = blueCanvas.getContext("2d");
        var blueImageData = new ImageData(image.width, image.height);
        blueCanvas.width = image.width;
        blueCanvas.height = image.height;

        redCanvasContext.drawImage(image, 0, 0);
        var rawImageData = redCanvasContext.getImageData(0, 0, image.width, image.height);
        for (var i = 0; i < rawImageData.data.length; i += 4) {
            redImageData.data[i] = rawImageData.data[i];
            redImageData.data[i + 3] = rawImageData.data[i + 3];
            greenImageData.data[i + 1] = rawImageData.data[i + 1];
            greenImageData.data[i + 3] = rawImageData.data[i + 3];
            blueImageData.data[i + 2] = rawImageData.data[i + 2];
            blueImageData.data[i + 3] = rawImageData.data[i + 3];
        }

        redCanvasContext.putImageData(redImageData, 0, 0);
        greenCanvasContext.putImageData(greenImageData, 0, 0);
        blueCanvasContext.putImageData(blueImageData, 0, 0);
        decomposedBitmaps[bitmapNames[index]] = {red: redCanvas, green: greenCanvas, blue: blueCanvas};

        if (imagesHandled == preloader.urls.length) {
            init4();
        }
    };
    var errorCallback = function(preloader, imagesHandled, index) {
        drawLoadingMessage("whoops, couldn't load the pic \"" + preloader.keys[index] + "\"");
        return true;
    };

    var urls = [];
    for (var i = 0; i < bitmapNames.length; i++) {
        urls.push(bitmapNames[i] + ".png");
    }
    // loadCallback processes all the bitmaps into a different data structure,
    // so we don't need to keep the preloader around after.
    var imagePreloader = new ImagePreloader(urls, {base: "graphics/", keys: bitmapNames, loadCallback: loadCallback, errorCallback: errorCallback});
    drawLoadingMessage("requesting pics");
}

function init4() {
    gameCanvasContext.font = font;
    characterWidth = gameCanvasContext.measureText("a").width;

    gameCanvas.addEventListener("keydown", keyDownHandler);
    gameCanvas.addEventListener("keyup", keyUpHandler);
    gameCanvas.addEventListener("focus", function(event) {
        if (raf === undefined) {
            musicEngine.play();
            draw();
        }
    });
    gameCanvas.addEventListener("blur", function(event) { gameCanvas.blur(); });
    gameCanvas.tabIndex = 0;

    statusCanvas.tabIndex = 0;
    statusCanvas.addEventListener("focus", function(event) { gameCanvas.focus(); event.preventDefault(); });

    scenes.push(new TitleSceneController());
    scenes.push(new GameSceneController());
    scenes.push(new EndingSceneController());
    scenes[currentScene].startTransitionIn();
    scenes[currentScene].finishTransitionIn();

    // Start off by pausing the game, in case the canvas doesn't have focus
    // immediately. This can happen if the game loads while the window is in
    // background.
    musicEngine.loadLoopsAndPlay();
    musicEngine.pause();
    drawPauseOverlay();

    // Also calls draw() per event listener.
    gameCanvas.focus();
}


//
// GAME LOGIC
//

function setMusicChaos(newChaos) {
    // Round to the closest 0.05 to reduce update frequency when moving.
    // Helps prevent audio crackling from constant resyncing.
    newChaos = Math.round(newChaos * 20) / 20;

    if (musicChaos != newChaos) {
        musicChaos = newChaos;
        musicEngine.setChaos(musicChaos);
    }
}

function uncertainty(chaos) {
    return chaos * chaosMaxUncertainty;
}

function chaosOffset(chaos) {
    return randomFloatInRange(-uncertainty(chaos), chaos * uncertainty(chaos));
}

function startTransition(destination) {
    scenes[currentScene].startTransitionOut();
    sceneTransitionDestination = destination;
    sceneTransitionTicks = sceneTransitionDuration;
    sceneTransitionDirection = "out";
}

function overlap(a, b) {
    return ((a.left <= b.right) && (a.right >= b.left) && (a.top <= b.bottom) && (a.bottom >= b.top));
}


//
// DRAWING
//

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

function clearScreen() {
    gameCanvasContext.globalCompositeOperation = "source-over";
    gameCanvasContext.fillStyle = "black";
    gameCanvasContext.fillRect(0, 0, gameWidth, gameHeight);
    statusCanvasContext.globalCompositeOperation = "source-over";
    statusCanvasContext.fillStyle = "black";
    statusCanvasContext.fillRect(0, 0, gameWidth, statusHeight);
}

function drawLoadingMessage(text) {
    clearScreen();
    drawTextCenteredAt(text, gameCanvasContext, gameWidth / 2, (gameHeight + statusHeight) / 2, 0);
}

function drawFadeOverlay(fade) {
    gameCanvasContext.globalCompositeOperation = "source-over";
    gameCanvasContext.fillStyle = "rgba(0, 0, 0, " + fade + ")";
    gameCanvasContext.fillRect(0, 0, gameWidth, gameHeight);

    statusCanvasContext.globalCompositeOperation = "source-over";
    statusCanvasContext.fillStyle = "rgba(0, 0, 0, " + fade + ")";
    statusCanvasContext.fillRect(0, 0, gameWidth, statusHeight);
}

function drawPauseOverlay() {
    drawFadeOverlay(0.5);

    statusCanvasContext.fillStyle = "white";
    statusCanvasContext.font = "96px sans-serif";
    var metrics = statusCanvasContext.measureText("(click)");
    // Firefox doesn't support the height metrics yet :(
    statusCanvasContext.fillText("(click)", Math.round((gameWidth - metrics.width) / 2), 100);
}

function draw(timestamp) {
    if (document.activeElement != gameCanvas) {
        raf = undefined;
        drawPauseOverlay();
        musicEngine.pause();
        return;
    }

    clearScreen();
    scenes[currentScene].draw(timestamp);

    if (sceneTransitionDestination != -1) {
        sceneTransitionTicks -= 1;
        if (sceneTransitionDirection == "out") {
            drawFadeOverlay((sceneTransitionDuration - sceneTransitionTicks) / sceneTransitionDuration);
        } else {
            drawFadeOverlay(sceneTransitionTicks / sceneTransitionDuration);
        }

        if (sceneTransitionTicks == 0) {
            if (sceneTransitionDirection == "out") {
                scenes[currentScene].finishTransitionOut();
                scenes[sceneTransitionDestination].startTransitionIn();
                currentScene = sceneTransitionDestination;
                sceneTransitionTicks = sceneTransitionDuration;
                sceneTransitionDirection = "in";
            } else {
                scenes[sceneTransitionDestination].finishTransitionIn();
                sceneTransitionDestination = -1;
            }
        }
    }

    raf = window.requestAnimationFrame(draw);
}

function drawSprite(name, context, x, y, chaos) {
    context.globalCompositeOperation = "lighten";
    context.drawImage(decomposedBitmaps[name].red, Math.round(x + chaosOffset(chaos)), Math.round(y + chaosOffset(chaos)));
    context.drawImage(decomposedBitmaps[name].green, Math.round(x + chaosOffset(chaos)), Math.round(y + chaosOffset(chaos)));
    context.drawImage(decomposedBitmaps[name].blue, Math.round(x + chaosOffset(chaos)), Math.round(y + chaosOffset(chaos)));
}

function drawTexts(texts, context, x, y, chaos) {
    for (var i = 0; i < texts.length; i++) {
        var text = texts[i];
        drawText(text.text, context, text.x, text.y, (text.chaos === undefined) ? chaos : text.chaos);
    }
}

function drawText(text, context, x, y, chaos) {
    for (var i = 0; i < text.length; i++) {
        if (possibleScrambleCharacters.includes(text[i]) && randomFloatInRange(0, 1) < chaos) {
            text = text.substr(0, i) + possibleScrambleCharacters[randomIntInRange(0, possibleScrambleCharacters.length - 1)] + text.substr(i + 1);
        }
    }

    context.globalCompositeOperation = "lighten";
    context.font = font;

    context.fillStyle = "#FF0000";
    context.fillText(text, Math.round(x + chaosOffset(chaos)), Math.round(y + chaosOffset(chaos)));
    context.fillStyle = "#00FF00";
    context.fillText(text, Math.round(x + chaosOffset(chaos)), Math.round(y + chaosOffset(chaos)));
    context.fillStyle = "#0000FF";
    context.fillText(text, Math.round(x + chaosOffset(chaos)), Math.round(y + chaosOffset(chaos)));
}

function drawTextCenteredAt(text, context, x, y, chaos) {
    context.font = font;
    var metrics = context.measureText(text);
    drawText(text, context, x - metrics.width / 2, y, chaos);
}

function splitText(text, x, y, width) {
    var lineWidth = Math.floor(width / characterWidth);
    var words = text.split(" ");

    // Make one chunk for every word in the input.
    var chunks = [];
    var currentLineWidth = 0;
    for (var i = 0; i < words.length; i++) {
        if (words[i] == "{NL}") {
            currentLineWidth = 0;
            y += lineHeight;
        } else {
            var chunk = {text: words[i]};
            if (chunk.text[0] == "{") {
                var s = chunk.text.slice(1).split("}");
                chunk.chaos = parseFloat(s[0]);
                chunk.text = s[1];
            }
            if (currentLineWidth + chunk.text.length > lineWidth) {
                currentLineWidth = 0;
                y += lineHeight;
            }
            chunk.x = x + currentLineWidth * characterWidth;
            currentLineWidth += chunk.text.length + 1;
            chunk.y = y;
            chunks.push(chunk);
        }
    }

    // Merge consecutive chunks if they appear on the same line.
    var i = 0;
    while (i < (chunks.length - 1)) {
        if (chunks[i].y == chunks[i + 1].y && chunks[i].chaos == chunks[i + 1].chaos) {
            var newChunk = Object.assign({}, chunks[i]);
            newChunk.text = newChunk.text + " " + chunks[i + 1].text;
            chunks[i] = newChunk;
            chunks.splice(i + 1, 1);
        } else {
            i += 1;
        }
    }

    return chunks;
}


//
// INPUT
//

function keyDownHandler(event) {
    if (eatenKeyCodes.includes(event.keyCode)) {
        keyDown[event.keyCode] = true;
        event.preventDefault();

        if (event.keyCode == 90) {
            lastActionKeyPressed = "[Z]";
            scenes[currentScene].triggerAction();
        } else if (event.keyCode == 88) {
            lastActionKeyPressed = "[X]";
            scenes[currentScene].triggerAction();
        } else if (event.keyCode == 32) {
            lastActionKeyPressed = "[SPACE]";
            scenes[currentScene].triggerAction();
        }
    }
}

function keyUpHandler(event) {
    if (eatenKeyCodes.includes(event.keyCode)) {
        keyDown[event.keyCode] = false;
        event.preventDefault();
    }
}

function upKeyDown() {
    return (keyDown[38] || keyDown[87] || keyDown[73]);
}

function downKeyDown() {
    return (keyDown[40] || keyDown[83] || keyDown[75]);
}

function leftKeyDown() {
    return (keyDown[37] || keyDown[65] || keyDown[74]);
}

function rightKeyDown() {
    return (keyDown[39] || keyDown[68] || keyDown[76]);
}

function movementKeyDown() {
    return (upKeyDown() || downKeyDown() || leftKeyDown() || rightKeyDown());
}

function actionKeyDown() {
    return (keyDown[90] || keyDown[88] || keyDown[32]);
}

function quitKeyDown() {
    return keyDown[27];
}
