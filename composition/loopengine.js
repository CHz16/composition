// loopengine.js
// Music engine that randomly selects a number of background loops to play
// together, desychronizing them in a controlled manner according to a given
// intensity setting.
//
// The first two arguments are an array of AudioBuffers and the AudioContext
// they were created in.
//
// The third optional argument is an object with any or all of the following
// properties set:
//
//   - maxChannels: The max number of loops to play simultaneously. Default = 4
//
//   - chaos: A chaos factor to start at. Default = 0.0
function LoopEngine(loopBuffers, context, args) {
    this.createChannels = function() {
        for (var i = 0; i < this.channelBufferIndices.length && i < this.maxChannels; i++) {
            var duration = loopBuffers[this.channelBufferIndices[i]].duration;
            var offset = context.currentTime - this.syncTime;
            offset = offset - Math.floor(offset / duration) * duration;

            var source = this.context.createBufferSource();
            this.channels[i] = source;
            source.loop = true;
            source.buffer = this.loopBuffers[this.channelBufferIndices[i]];
            source.connect(this.context.destination);
            source.start(0, offset);

            this.twiddleDetune(i);
        }
    }

    this.twiddleDetune = function(channel) {
        this.channels[channel].detune.value = randomFloatInRange(-500 * this.chaos, 500 * this.chaos);

        this.channelTimeouts[channel] = window.setTimeout(this.twiddleDetune.bind(this), 500, channel);
    }

    this.killAllChannels = function() {
        for (var channel = 0; channel < this.channels.length; channel++) {
            window.clearTimeout(this.channelTimeouts[channel]);
            delete this.channelTimeouts[channel];

            this.channels[channel].stop();
            this.channels[channel].disconnect();
            delete this.channels[channel];
        }
    }


    // Grab all the arguments from the constructor.
    this.loopBuffers = loopBuffers;
    this.context = context;
    args = args || new Object();
    this.maxChannels = args.maxChannels || 4;
    this.chaos = args.chaos || 0.0;

    // Set up additional data.
    this.isPlaying = false;
    this.syncTime = 0;
    this.pauseTime = 0;
    this.channels = [];
    this.channelBufferIndices = [];
    this.channelTimeouts = [];
}

// Loads a new set of loops and starts playing them immediately. If an array of
// indices is passed as an argument, then those specific loops will be loaded.
// If not, a random set of maxChannels loops will be loaded.
LoopEngine.prototype.loadLoopsAndPlay = function(bufferIndices) {
    if (this.isPlaying) {
        this.pause();
    }

    if (!bufferIndices) {
        var indices = [];
        for (var i = 0; i < this.loopBuffers.length; i++) {
            indices.push(i);
        }
        bufferIndices = shuffle(indices).slice(0, this.maxChannels);
    }
    this.channelBufferIndices = bufferIndices;

    this.syncTime = this.context.currentTime;
    this.pauseTime = this.syncTime;
    this.play();
}

// If the engine is paused, then start playing music again.
LoopEngine.prototype.play = function() {
    if (this.isPlaying) {
        return;
    }
    this.isPlaying = true;

    this.syncTime = this.syncTime + (this.context.currentTime - this.pauseTime);
    this.createChannels();
}

// If the engine is playing music, then pause playback.
LoopEngine.prototype.pause = function() {
    if (!this.isPlaying) {
        return;
    }
    this.isPlaying = false;

    this.killAllChannels();
    this.pauseTime = this.context.currentTime;
}

// Change the chaos level of the music engine.
LoopEngine.prototype.setChaos = function(chaos) {
    if (chaos > 1) {
        chaos = 1;
    } else if (chaos < 0) {
        chaos = 0;
    }
    this.chaos = chaos;

    if (this.isPlaying) {
        this.pause();
        this.play();
    }
}
