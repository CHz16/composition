// utilities.js
// Some random functions I'm splitting out into a different file. This only
// ended up being random number stuff whoops


//
// RANDOMNESS
//

function shuffle(a) {
    for (var i = 0; i < a.length - 1; i++) {
        var temp = a[i];
        var swapIndex = randomIntInRange(i, a.length - 1);
        a[i] = a[swapIndex];
        a[swapIndex] = temp;
    }
    
    return a;
}

function randomIntInRange(min, max) {
    return min + Math.floor((max - min + 1) * Math.random());
}

function randomFloatInRange(min, max) {
    return min + (max - min) * Math.random();
}
