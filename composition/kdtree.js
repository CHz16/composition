// kdtree.js
// Simple k-d tree implementation. Doesn't do balancing or anything like that.


function KDTree(objects, rect, minQuadrantSize) {
    this.rect = Object.assign({}, rect);
    this.midX = Math.floor((this.rect.right + this.rect.left) / 2);
    this.midY = Math.floor((this.rect.bottom + this.rect.top) / 2);
    
    this.objects = [];
    this.isLeaf = (this.midX - this.rect.left) < minQuadrantSize || (this.midY - this.rect.top) < minQuadrantSize;
    if (!this.isLeaf) {
        this.upperLeftRect = {top: this.rect.top, bottom: this.midY, left: this.rect.left, right: this.midX};
        this.upperLeft = new KDTree([], this.upperLeftRect, minQuadrantSize);
        this.upperRightRect = {top: this.rect.top, bottom: this.midY, left: this.midX + 1, right: this.rect.right};
        this.upperRight = new KDTree([], this.upperRightRect, minQuadrantSize);
        this.lowerLeftRect = {top: this.midY + 1, bottom: this.rect.bottom, left: this.rect.left, right: this.midX};
        this.lowerLeft = new KDTree([], this.lowerLeftRect, minQuadrantSize);
        this.lowerRightRect = {top: this.midY + 1, bottom: this.rect.bottom, left: this.midX + 1, right: this.rect.right};
        this.lowerRight = new KDTree([], this.lowerRightRect, minQuadrantSize);
    }
    
    for (var i = 0; i < objects.length; i++) {
        this.insert(objects[i]);
    }
}

KDTree.prototype.insert = function(object) {
    if (this.isLeaf) {
        this.objects.push(object);
        return;
    }
    
    var ul = overlap(this.upperLeftRect, object) ? 1 : 0;
    var ur = overlap(this.upperRightRect, object) ? 1 : 0;
    var ll = overlap(this.lowerLeftRect, object) ? 1 : 0;
    var lr = overlap(this.lowerRightRect, object) ? 1 : 0;
    
    if (ul + ur + ll + lr < 2) {
        if (ul) {
            this.upperLeft.insert(object);
        } else if (ur) {
            this.upperRight.insert(object);
        } else if (ll) {
            this.lowerLeft.insert(object);
        } else if (lr) {
            this.lowerRight.insert(object);
        }
    } else {
        this.objects.push(object);
    }
}

KDTree.prototype.objectsOverlapping = function(object) {
    var os = [];
    if (!this.isLeaf) {
        if (overlap(this.upperLeftRect, object)) {
            os = this.upperLeft.objectsOverlapping(object);
        }
        if (overlap(this.upperRightRect, object)) {
            os = os.concat(this.upperRight.objectsOverlapping(object));
        }
        if (overlap(this.lowerLeftRect, object)) {
            os = os.concat(this.lowerLeft.objectsOverlapping(object));
        }
        if (overlap(this.lowerRightRect, object)) {
            os = os.concat(this.lowerRight.objectsOverlapping(object));
        }
    }
    
    for (var i = 0; i < this.objects.length; i++) {
        if (overlap(object, this.objects[i])) {
            os.push(this.objects[i]);
        }
    }
    return os;
}