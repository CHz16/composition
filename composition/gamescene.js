// gamescene.js
// Controller for the game scene.
function GameSceneController() {
    this.loadObjects = function() {
        for (var row = 0; row < map.length; row++) {
            for (var col = 0; col < map[0].length; col++) {
                if (map[row][col] == 1) {
                    this.playerStartX = col * tileSize;
                    this.playerStartY = row * tileSize;
                } else if (map[row][col] == 2) {
                    this.radioStartX = col * tileSize;
                    this.radioStartY = row * tileSize;
                } else if (map[row][col] != 0) {
                    var entityData = entities[map[row][col]];
                    entityData.horizSpan = entityData.horizSpan || 1;
                    entityData.vertSpan = entityData.vertSpan || 1;
                    var definition = definitions[entityData.definition];
                    var object = {definition: entityData.definition};
                    
                    if (entityData.rotate) {
                        var autogenDefinitionName = "entity" + map[row][col];
                        var newDefinition = Object.assign({}, definition);
                        
                        var sourceWidth = decomposedBitmaps[definition.image].red.width;
                        var sourceHeight = decomposedBitmaps[definition.image].red.height;
                        var newWidth = sourceHeight, newHeight = sourceWidth;
                        var xOffset = 0, yOffset = 0;
                        if (entityData.rotate == 90) {
                            var temp = entityData.horizSpan;
                            entityData.horizSpan = entityData.vertSpan;
                            entityData.vertSpan = temp;
                            
                            newDefinition.topOffset = definition.leftOffset;
                            newDefinition.leftOffset = definition.bottomOffset;
                            newDefinition.bottomOffset = definition.rightOffset;
                            newDefinition.rightOffset = definition.topOffset;
                            
                            yOffset = -sourceHeight;
                        } else if (entityData.rotate == 180) {
                            newDefinition.topOffset = definition.bottomOffset;
                            newDefinition.bottomOffset = definition.topOffset;
                            newDefinition.leftOffset = definition.rightOffset;
                            newDefinition.rightOffset = definition.leftOffset;
                            
                            xOffset = -sourceWidth;
                            yOffset = -sourceHeight;
                            newWidth = sourceWidth;
                            newHeight = sourceHeight;
                        } else if (entityData.rotate == 270) {
                            var temp = entityData.horizSpan;
                            entityData.horizSpan = entityData.vertSpan;
                            entityData.vertSpan = temp;
                            
                            newDefinition.topOffset = definition.rightOffset;
                            newDefinition.rightOffset = definition.bottomOffset;
                            newDefinition.bottomOffset = definition.leftOffset;
                            newDefinition.leftOffset = definition.topOffset;
                            
                            xOffset = -sourceWidth;
                        }
                        
                        var newBitmap = new Object();
                        var channels = ["red", "green", "blue"];
                        for (var c = 0; c < channels.length; c++) {
                            var sourceCanvas = decomposedBitmaps[definition.image][channels[c]];
                            var canvas = document.createElement("canvas");
                            var canvasContext = canvas.getContext("2d");
                            canvas.width = newWidth;
                            canvas.height = newHeight;
                            canvasContext.rotate(entityData.rotate * Math.PI / 180);
                            canvasContext.drawImage(sourceCanvas, xOffset, yOffset);
                            canvasContext.setTransform(1, 0, 0, 1, 0, 0);
                            newBitmap[channels[c]] = canvas;
                        }
                        decomposedBitmaps[autogenDefinitionName] = newBitmap;
    
                        newDefinition.image = autogenDefinitionName;
                        definitions[autogenDefinitionName] = newDefinition;
                        entityData.definition = autogenDefinitionName;
                        delete entityData.rotate;
                        
                        object.definition = autogenDefinitionName;
                        definition = newDefinition;
                    }
                    
                    if (entityData.repeat) {
                        var autogenDefinitionName = "entity" + map[row][col];
                        var newDefinition = Object.assign({}, definition);
                        
                        var sourceWidth = decomposedBitmaps[definition.image].red.width;
                        var sourceHeight = decomposedBitmaps[definition.image].red.height;
                        var newWidth = sourceWidth + (entityData.repeat[0] - 1) * (tileSize * entityData.horizSpan);
                        var newHeight = sourceHeight + (entityData.repeat[1] - 1) * (tileSize * entityData.vertSpan);
                        
                        var newBitmap = new Object();
                        var channels = ["red", "green", "blue"];
                        for (var c = 0; c < channels.length; c++) {
                            var sourceCanvas = decomposedBitmaps[definition.image][channels[c]];
                            var canvas = document.createElement("canvas");
                            var canvasContext = canvas.getContext("2d");
                            canvas.width = newWidth;
                            canvas.height = newHeight;
                            for (var tr = 0; tr < entityData.repeat[1]; tr++) {
                                for (var tc = 0; tc < entityData.repeat[0]; tc++) {
                                    canvasContext.drawImage(sourceCanvas, (tc * entityData.horizSpan) * tileSize, (tr * entityData.vertSpan) * tileSize);
                                }
                            }
                            newBitmap[channels[c]] = canvas;
                        }
                        decomposedBitmaps[autogenDefinitionName] = newBitmap;
    
                        newDefinition.image = autogenDefinitionName;
                        definitions[autogenDefinitionName] = newDefinition;
                        entityData.definition = autogenDefinitionName;
                        entityData.horizSpan *= entityData.repeat[0];
                        entityData.vertSpan *= entityData.repeat[1];
                        delete entityData.repeat;
                        
                        object.definition = autogenDefinitionName;
                        definition = newDefinition;
                    }
                    
                    object.top = row * tileSize;
                    object.top += definition.topOffset || 0;
                    object.bottom = (row + entityData.vertSpan) * tileSize - 1;
                    object.bottom -= definition.bottomOffset || 0;
                    object.left = col * tileSize;
                    object.left += definition.leftOffset || 0;
                    object.right = (col + entityData.horizSpan) * tileSize - 1;
                    object.right -= definition.rightOffset || 0;
                    
                    this.objects.push(object);
                }
            }
        }
        this.kdTree = new KDTree(this.objects, {top: 0, bottom: map.length * tileSize, left: 0, right: map[0].length * tileSize}, tileSize);
    }
    
    this.repositionViewport = function() {
        var mapWidth = map[0].length * tileSize;
        var mapHeight = map.length * tileSize;
        
        this.viewportLeft = this.playerX - tileSize / 2 - gameWidth / 2;
        if (this.viewportLeft < 0) {
            this.viewportLeft = 0;
        } else if ((this.viewportLeft + gameWidth) >= mapWidth) {
            this.viewportLeft = mapWidth - gameWidth;
        }
        this.viewportRight = this.viewportLeft + gameWidth;
        
        this.viewportTop = this.playerY - tileSize / 2 - gameHeight / 2;
        if (this.viewportTop < 0) {
            this.viewportTop = 0;
        } else if ((this.viewportTop + gameHeight) >= mapHeight) {
            this.viewportTop = mapHeight - gameHeight;
        }
        this.viewportBottom = this.viewportTop + gameHeight;
    }
    
    this.drawSpriteInViewport = function(name, context, x, y, chaos) {
        drawSprite(name, context, x - this.viewportLeft, y - this.viewportTop, chaos);
    }
    
    this.drawInventory = function() {
        drawTextCenteredAt("Inside you:", statusCanvasContext, inventoryCenterLine, statusTextStartY, this.chaos);
        for (var i = 0; i < 5; i++) {
            var s = "";
            var chaos = this.chaos;
            if (i == 4) {
                if (this.actionState == "taking") {
                    s = "(Do not)";
                } else {
                    s = "";
                }
            } else if (this.inventory[i]) {
                s = types[definitions[this.inventory[i].object.definition].type].name;
                chaos = this.inventory[i].chaos;
            }
            if (this.actionState == "taking" && this.takeSelection == i) {
                s = "  " + s;
                drawTextCenteredAt(">" + " ".repeat(s.length - 1), statusCanvasContext, inventoryCenterLine, statusTextStartY + lineHeight * (i + 1), this.chaos);
            }
            drawTextCenteredAt(s, statusCanvasContext, inventoryCenterLine, statusTextStartY + lineHeight * (i + 1), chaos);
        }
    }
    
    // There may be some off-by-one errors here, I'm writing this quickly
    this.allObjectsOverlappingRect = function(rect, chaos) {
        var currentUncertainty = uncertainty(chaos);
        rect.top -= currentUncertainty;
        rect.bottom += currentUncertainty;
        rect.left -= currentUncertainty;
        rect.right += currentUncertainty;
        return this.kdTree.objectsOverlapping(rect);
    }
    
    this.makeRadio = function(x, y, facing) {
        facing = facing || this.playerFacing;
        x += facing[0] * tileSize;
        y += facing[1] * tileSize;
        
        return {definition: "radio", top: y, bottom: y + decomposedBitmaps["radio"].red.height - 1, left: x, right: x + decomposedBitmaps["radio"].red.width - 1};
    }
    
    this.checkRadioSpace = function() {
        if (this.actionState != "carrying") {
            this.canDropRadio = false;
        } else {
            var radioCheck = this.makeRadio(this.playerX, this.playerY);
            this.canDropRadio = (this.inBounds(radioCheck) && this.allObjectsOverlappingRect(radioCheck, 0).length == 0);
        }
    }
    
    this.inBounds = function(rect) {
        return (rect.top >= 0 && rect.bottom < map.length * tileSize && rect.left >= 0 && rect.right < map[0].length * tileSize);
    }
    
    this.findVisibleObjects = function() {
        this.visibleObjects = (this.radio) ? [this.radio] : [];
        this.visibleObjects = this.visibleObjects.concat(this.allObjectsOverlappingRect({top: this.viewportTop, bottom: this.viewportBottom, left: this.viewportLeft, right: this.viewportRight}, this.chaos));
    }
    
    this.move = function(timestamp) {
        if (this.actionState == "feeling") {
            return false;
        } else if (this.actionState == "taking") {
            if (this.menuLastMoved + keyDelay >= timestamp) {
                return false;
            }
            this.menuLastMoved = timestamp;
            if (upKeyDown() && this.takeSelection >= 0) {
                this.takeSelection = (this.takeSelection + 4) % 5;
            } else if (downKeyDown() && this.takeSelection >= 0) {
                this.takeSelection = (this.takeSelection + 1) % 5;
            }
            return false;
        } else if (this.actionState == "leaving") {
            if (this.menuLastMoved + keyDelay >= timestamp) {
                return false;
            }
            this.menuLastMoved = timestamp;
            if (upKeyDown() || downKeyDown()) {
                this.leavingSelection = !this.leavingSelection;
            }
            return false;
        }
        
        var sightRectangles = [];
        
        // Update the player's position.
        var xVelocity = 0, yVelocity = 0;
        if (upKeyDown()) {
            // up
            yVelocity -= playerSpeed;
        }
        if (downKeyDown()) {
            // down
            yVelocity += playerSpeed;
        }
        if (leftKeyDown()) {
            // left
            xVelocity -= playerSpeed;
        } 
        if (rightKeyDown()) {
            // right
            xVelocity += playerSpeed;
        }
        this.playerFacing = [0, 0];
        if (xVelocity != 0) {
            this.playerFacing[0] = Math.sign(xVelocity) * xVelocity / xVelocity;
        } else if (yVelocity != 0) {
            this.playerFacing[1] = Math.sign(yVelocity) * yVelocity / yVelocity;
        }
        if (xVelocity != 0 && yVelocity != 0) {
            xVelocity /= Math.sqrt(2);
            yVelocity /= Math.sqrt(2);
        }
        
        if (xVelocity != 0 || yVelocity != 0) {
            // Simple collision detection with wall ejection, horiz then vert
            var newLocation = {top: this.playerY, bottom: this.playerY + tileSize - 1, left: this.playerX + xVelocity, right: this.playerX + xVelocity + tileSize - 1};
            for (var i = 0; i < this.visibleObjects.length; i++) {
                var o = this.visibleObjects[i];
                if (overlap(newLocation, o)) {
                    if (newLocation.left > o.left) {
                        newLocation.left = o.right + 1;
                        newLocation.right = o.right + tileSize;
                    } else {
                        newLocation.right = o.left - 1;
                        newLocation.left = o.left - tileSize;
                    }
                }
            }
            
            newLocation.top = this.playerY + yVelocity;
            newLocation.bottom = this.playerY + yVelocity + tileSize - 1;
            for (var i = 0; i < this.visibleObjects.length; i++) {
                var o = this.visibleObjects[i];
                if (overlap(newLocation, o)) {
                    if (newLocation.top > o.top) {
                        newLocation.top = o.bottom + 1;
                        newLocation.bottom = o.bottom + tileSize;
                    } else {
                        newLocation.bottom = o.top - 1;
                        newLocation.top = o.top - tileSize;
                    }
                }
            }
            
            // Finally we can move the player.
            var moved = false;
            if (this.playerX != newLocation.left || this.playerY != newLocation.top) {
                if (this.inBounds(newLocation)) {
                    this.playerX = newLocation.left;
                    this.playerY = newLocation.top;
                    moved = true;
                } else {
                    this.leavingSelection = false;
                    this.actionState = "leaving";
                }
            }
            
            // See if there's space to drop the radio.
            if (this.actionState == "carrying") {
                this.checkRadioSpace();
            }
            
            // Check if we're facing something.
            if (xVelocity < 0) {
                sightRectangles.push({top: this.playerY + tileSize / 2 - 5, bottom: this.playerY + tileSize / 2 + 4, left: this.playerX - 1, right: this.playerX - 1});
            } else if (xVelocity > 0) {
                sightRectangles.push({top: this.playerY + tileSize / 2 - 5, bottom: this.playerY + tileSize / 2 + 4, left: this.playerX + tileSize, right: this.playerX + tileSize});
            }
            if (yVelocity < 0) {
                sightRectangles.push({top: this.playerY - 1, bottom: this.playerY - 1, left: this.playerX + tileSize / 2 - 5, right: this.playerX + tileSize / 2 + 4});
            } else if (yVelocity > 0) {
                sightRectangles.push({top: this.playerY + tileSize, bottom: this.playerY + tileSize, left: this.playerX + tileSize / 2 - 5, right: this.playerX + tileSize / 2 + 4});
            }
            if (sightRectangles.length > 0) {
                this.lastViewedObject = -1;
            }
            for (var i = 0; i < sightRectangles.length; i++) {
                if (this.radio && overlap(sightRectangles[i], this.radio)) {
                    this.lastViewedObject = this.radio;
                    break;
                }
                var os = this.allObjectsOverlappingRect(sightRectangles[i], 0);
                if (os.length > 0) {
                    this.lastViewedObject = os[0];
                    break;
                }
            }
            
            return moved;
        }
        return false;
    }
    
    this.act = function(timestamp) {
        if (this.actionState == "nothing" && this.lastViewedObject != -1) {
            if (this.lastViewedObject.definition == "radio") {
                this.actionState = "carrying";
                this.radio = undefined;
                this.checkRadioSpace();
                this.lastViewedObject = -1;
                this.visibleObjects = this.visibleObjects.slice(1);
                return;
            }
            this.actionState = "feeling";
            this.feelPages = types[definitions[this.lastViewedObject.definition].type].description.split("{P}");
            this.feelPage = 0;
            this.feelTexts = splitText(this.feelPages[0] + " " + lastActionKeyPressed, statusTextStartX, statusTextStartY, statusTextWidth);
        } else if (this.actionState == "feeling") {
            if (this.feelPage < (this.feelPages.length - 1)) {
                this.feelPage += 1
                this.feelTexts = splitText(this.feelPages[this.feelPage] + " " + lastActionKeyPressed, statusTextStartX, statusTextStartY, statusTextWidth);
            } else {
                this.actionState = "taking";
                this.takeSelection = 0;
            }
        } else if (this.actionState == "taking") {
            if (this.takeSelection < 4) {
                for (var i = 0; i < 4; i++) {
                    if (i == this.takeSelection) {
                        continue;
                    }
                    if (this.inventory[i] && definitions[this.inventory[i].object.definition].type == definitions[this.lastViewedObject.definition].type) {
                        this.inventory[i] = undefined;
                    }
                }
                this.inventory[this.takeSelection] = {object: this.lastViewedObject, chaos: this.chaos};
            } else {
                for (var i = 0; i < 4; i++) {
                    if (this.inventory[i] && definitions[this.inventory[i].object.definition].type == definitions[this.lastViewedObject.definition].type) {
                        this.inventory[i] = undefined;
                        break;
                    }
                }
            }
            this.actionState = "nothing";
        } else if (this.actionState == "carrying" && this.canDropRadio) {
            this.radio = this.makeRadio(this.playerX, this.playerY);
            this.canDropRadio = false;
            this.visibleObjects = [this.radio].concat(this.visibleObjects);
            this.lastViewedObject = this.radio;
            this.actionState = "nothing";
            musicEngine.loadLoopsAndPlay();
        } else if (this.actionState == "leaving") {
            if (!this.leavingSelection) {
                this.actionState = (this.radio) ? "nothing" : "carrying";
            } else {
                startTransition(2);
            }
        }
    }
    
    // Member variable declarations, don't pay attention to these values.
    // Initialized properly in startTransitionIn().
    this.chaos = 0;
    this.playerX = 0;
    this.playerY = 0;
    this.playerFacing = [];
    this.radio = {};
    this.canDropRadio = false;
    this.viewportTop = 0;
    this.viewportBottom = 0;
    this.viewportLeft = 0;
    this.viewportRight = 0;
    this.objects = [];
    this.visibleObjects = [];
    this.kdTree = undefined;
    this.lastViewedObject = -1;
    this.transitioning = true;
    this.menuLastMoved = 0;
    this.actionState = "nothing";
    this.actionPending = false;
    this.feelPages = [], this.feelPage = 0, this.feelTexts = [];
    this.takeSelection = 0;
    this.inventory = [];
    this.leavingSelection = false;
    this.elapsedFrames = 0;
    this.perturbationLevel = 0, this.chaosWaveCycles = 0;
}

GameSceneController.prototype.draw = function(timestamp) {
    // If we're not transitioning, handle player input.
    var moved = false;
    if (!this.transitioning) {
        if (this.actionPending) {
            this.actionPending = false;
            this.act(timestamp);
        } else if (movementKeyDown()) {
            moved = this.move(timestamp);
        } else if (quitKeyDown()) {
            startTransition(0);
        }
    }
    
    var baseChaos = chaosWaveMaxChaos * (this.perturbationLevel / chaosWaveMaxPerturbation);
    var framesIntoCycle = this.elapsedFrames % (chaosWaveHoldDuration + chaosWaveHoldTransitionDuration * 2);
    if (framesIntoCycle < chaosWaveHoldDuration) {
        // baseChaos = baseChaos
    } else if (framesIntoCycle < chaosWaveHoldDuration + chaosWaveHoldTransitionDuration) {
        baseChaos += chaosWaveMaxChaos * (framesIntoCycle - chaosWaveHoldDuration) / chaosWaveHoldTransitionDuration;
    } else {
        baseChaos += chaosWaveMaxChaos * (1 - (framesIntoCycle - chaosWaveHoldDuration - chaosWaveHoldTransitionDuration) / chaosWaveHoldTransitionDuration);
    }
    // At the peak of the chaos wave, perturb the map as long as we're not
    // reading anything.
    if ((this.actionState == "nothing" || this.actionState == "carrying") && framesIntoCycle == chaosWaveHoldDuration + chaosWaveHoldTransitionDuration) {
        if (this.perturbationLevel != 0) {
            var playerRect = {left: this.playerX, right: this.playerX + tileSize - 1, top: this.playerY, bottom: this.playerY + tileSize - 1};
            for (var i = 0; i < this.objects.length; i++) {
                var vertOffset = randomIntInRange(-this.perturbationLevel, this.perturbationLevel);
                var horizOffset = randomIntInRange(-this.perturbationLevel, this.perturbationLevel);
                this.objects[i].top += vertOffset;
                this.objects[i].bottom += vertOffset;
                this.objects[i].left += horizOffset;
                this.objects[i].right += horizOffset;
                // Cancel if the new object's position clips into the player or radio.
                if (overlap(playerRect, this.objects[i]) || (this.radio && overlap(this.radio, this.objects[i]))) {
                    this.objects[i].top -= vertOffset;
                    this.objects[i].bottom -= vertOffset;
                    this.objects[i].left -= horizOffset;
                    this.objects[i].right -= horizOffset;
                }
            }
            this.kdTree = new KDTree(this.objects, {top: 0, bottom: map.length * tileSize, left: 0, right: map[0].length * tileSize}, tileSize);
            this.findVisibleObjects();
        }
        
        this.chaosWaveCycles += 1;
        if (this.chaosWaveCycles == chaosWaveCycleLength) {
            this.chaosWaveCycles = 0;
            this.perturbationLevel = Math.min(this.perturbationLevel + 1, chaosWaveMaxPerturbation);
        }
    }
    this.elapsedFrames += 1;
    
    if (this.radio) {
        var xDistance = (this.playerX + tileSize / 2) - (this.radio.right + this.radio.left) / 2;
        var yDistance = (this.playerY + tileSize / 2) - (this.radio.bottom + this.radio.top) / 2;
        var distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
        this.chaos = baseChaos + (distance - 50) / 500;
        if (this.chaos < baseChaos) {
            this.chaos = baseChaos;
        } else if (this.chaos > 1) {
            this.chaos = 1;
        }
    } else {
        this.chaos = baseChaos;
    }
    setMusicChaos(this.chaos);
    
    if (moved) {
        this.repositionViewport();
        this.findVisibleObjects();
    }
    
    // Draw the main window.
    this.drawSpriteInViewport("heart", gameCanvasContext, this.playerX, this.playerY, this.chaos);
    for (var i = 0; i < this.visibleObjects.length; i++) {
        var object = this.visibleObjects[i];
        this.drawSpriteInViewport(definitions[object.definition].image, gameCanvasContext, object.left, object.top, this.chaos);
    }
    
    // Draw the status window.
    var padding = 5;
    var radius = 10;
    
    statusCanvasContext.globalCompositeOperation = "lighten";
    statusCanvasContext.lineWidth = 3;
    var colors = ["#FF0000", "#00FF00", "#0000FF"];
    for (var i = 0; i < colors.length; i++) {
        var offsetX = chaosOffset(this.chaos);
        var offsetY = chaosOffset(this.chaos);
        statusCanvasContext.strokeStyle = colors[i];
        statusCanvasContext.beginPath();
        statusCanvasContext.moveTo(radius + padding + offsetX, padding + offsetY);
        statusCanvasContext.lineTo(gameWidth - radius - padding + offsetX, padding + offsetY);
        statusCanvasContext.arc(gameWidth - radius - padding + offsetX, radius + padding + offsetY, radius, 3 * Math.PI / 2, 2 * Math.PI);
        statusCanvasContext.lineTo(gameWidth - padding + offsetX, statusHeight - padding - radius + offsetY);
        statusCanvasContext.arc(gameWidth - radius - padding + offsetX, statusHeight - padding - radius + offsetY, radius, 0, Math.PI / 2);
        statusCanvasContext.lineTo(radius + padding + offsetX, statusHeight - padding + offsetY);
        statusCanvasContext.arc(radius + padding + offsetX, statusHeight - padding - radius + offsetY, radius, Math.PI / 2, Math.PI);
        statusCanvasContext.lineTo(padding + offsetX, radius + padding + offsetY);
        statusCanvasContext.arc(radius + padding + offsetX, radius + padding + offsetY, radius, Math.PI, 3 * Math.PI / 2);
        statusCanvasContext.stroke();
    }
    
    if (this.actionState == "nothing") {
        if (this.lastViewedObject != -1) {
            if (this.lastViewedObject.definition == "radio") {
                drawText(lastActionKeyPressed + " Pick up the radio", statusCanvasContext, statusTextStartX, statusTextStartY, this.chaos);
            } else {
                drawText(lastActionKeyPressed + " Feel " + types[definitions[this.lastViewedObject.definition].type].feelName, statusCanvasContext, statusTextStartX, statusTextStartY, this.chaos);
            }
        }
        this.drawInventory();
    } else if (this.actionState == "feeling") {
        drawTexts(this.feelTexts, statusCanvasContext, statusTextStartX, statusTextStartY, this.chaos);
    } else if (this.actionState == "taking") {
        drawText("Take " + types[definitions[this.lastViewedObject.definition].type].feelName + " with you?", statusCanvasContext, statusTextStartX, statusTextStartY, this.chaos);
        this.drawInventory();
    } else if (this.actionState == "carrying") {
        var lines = [];
        if (this.lastViewedObject != -1) {
            lines.push("Looking at " + types[definitions[this.lastViewedObject.definition].type].lookName);
            lines.push("(Put down the radio to feel it.)");
        }
        if (this.canDropRadio) {
            lines.push(lastActionKeyPressed + " Put down the radio");
        }
        
        var texts = splitText(lines.join(" {NL} "), statusTextStartX, statusTextStartY, statusTextWidth);
        drawTexts(texts, statusCanvasContext, statusTextStartX, statusTextStartY, this.chaos);
        this.drawInventory();
    } else if (this.actionState == "leaving") {
        var s = "Are you ready to leave? {NL} "
        if (!this.leavingSelection) {
            s += "> ";
        }
        s += " No {NL} ";
        if (this.leavingSelection) {
            s += "> ";
        }
        s += "Yes";
        var texts = splitText(s, statusTextStartX, statusTextStartY, statusTextWidth);
        drawTexts(texts, statusCanvasContext, statusTextStartX, statusTextStartY, this.chaos);
        this.drawInventory();
    }
}

GameSceneController.prototype.startTransitionIn = function() {
    this.transitioning = true;
    
    this.objects = [];
    this.visibleObjects = [];
    this.loadObjects();
    
    this.chaos = 0;
    this.playerX = this.playerStartX;
    this.playerY = this.playerStartY;
    this.playerFacing = [0, 0];
    this.radio = this.makeRadio(this.radioStartX, this.radioStartY, [0, 0]);
    this.checkRadioSpace();
    this.lastViewedObject = -1;
    this.actionState = "nothing";
    this.actionPending = false;
    this.menuLastMoved = 0;
    this.feelPages = [], this.feelPage = 0, this.feelTexts = [];
    this.takeSelection = 0;
    this.inventory = [];
    this.elapsedFrames = 0;
    this.perturbationLevel = 0, this.chaosWaveCycles = 0;
    
    this.repositionViewport();
    this.findVisibleObjects();
}

GameSceneController.prototype.finishTransitionIn = function() {
    this.transitioning = false;
}

GameSceneController.prototype.startTransitionOut = function() {
    this.transitioning = true;
}

GameSceneController.prototype.finishTransitionOut = function() {
    
}

GameSceneController.prototype.triggerAction = function() {
    this.actionPending = true;
}
