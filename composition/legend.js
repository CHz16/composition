// legend.js
// Game object definitions.

var currentYear = new Date().getFullYear();
var firstName = "{1}" + "a".repeat(randomIntInRange(3, 12));
var lastName = "{1}" + "a".repeat(randomIntInRange(3, 12));

var entities = [
    null, // 0: empty
    null, // 1: player
    null, // 2: radio
    {definition: "bush", horizSpan: 3}, // 3: horizontal bush
    {definition: "fence", repeat: [7, 1]}, // 4: horizontal fence x7
    {definition: "fencecorner"}, // 5: fence corner up/right
    {definition: "fence", repeat: [1, 11], rotate: 90}, // 6: vertical fence x11
    {definition: "fencecorner", rotate: 90}, // 7: fence corner down/right
    {definition: "fence", repeat: [11, 1]}, // 8: horizontal fence x11
    {definition: "fencecorner", rotate: 180}, // 9: fence corner down/left
    {definition: "fencecorner", rotate: 270}, // 10: fence corner up/left
    {definition: "fence"}, // 11: horizontal fence x1
    {definition: "yourhouse", horizSpan: 7, vertSpan: 6}, // 12: your house
    {definition: "garage", horizSpan: 3, vertSpan: 7}, // 13: garage
    {definition: "yourcar", vertSpan: 2}, // 14: your car
    {definition: "bush", rotate: 180, horizSpan: 3}, // 15: flipped horizontal bush
    {definition: "fencet"}, // 16: fence T flat down
    {definition: "fencet", rotate: 180}, // 17: fence T flat up
    {definition: "petershouse", horizSpan: 7, vertSpan: 6}, // 18: Peterses house
    {definition: "fence", repeat: [8, 1]}, // 19: horizontal fence x8
    {definition: "forsalesign", horizSpan: 2}, // 20: for sale sign
    {definition: "gilchristhouse", rotate: 180, horizSpan: 7, vertSpan: 6}, // 21: Gilchrist house
    {definition: "garage", rotate: 180, horizSpan: 3, vertSpan: 7}, // 22: flipped garage
    {definition: "car", rotate: 90, vertSpan: 2}, // 23: car facing right
    {definition: "tree", vertSpan: 3, horizSpan: 3}, // 24: tree
    {definition: "car", rotate: 180, vertSpan: 2}, // 25: car facing down
    {definition: "dufresnehouse", rotate: 180, horizSpan: 7, vertSpan: 6}, // 26: Dufresne house
    {definition: "pool", horizSpan: 4, vertSpan: 2}, // 27: pool
    {definition: "fence4way"}, // 28: fence +
    {definition: "fencet", rotate: 90}, // 29: fence T flat left
    {definition: "fencet", rotate: 270}, // 30: fence T flat right
    {definition: "huffhouse", rotate: 180, horizSpan: 7, vertSpan: 6}, // 31: Huff house
    {definition: "xinhouse", rotate: 180, horizSpan: 7, vertSpan: 6}, // 32: Xin house
    {definition: "bush", rotate: 90, horizSpan: 3}, // 33: vertical bush
    {definition: "bush", rotate: 270, horizSpan: 3}, // 34: vertical bush
    {definition: "newerhouse", horizSpan: 7, vertSpan: 6}, // 35: newer house
    {definition: "fence", repeat: [1, 10], rotate: 90}, // 36: vertical fence x10
    {definition: "gate", horizSpan: 3}, // 37: horizontal gate
    {definition: "car", vertSpan: 2}, // 38: car facing up
    {definition: "wall", rotate: 90, repeat: [1, 10]}, // 39: vertical wall x10
    {definition: "wallcornerwithfence"}, // 40: wall corner with fence flat down
    {definition: "wall", repeat: [11, 1]}, // 41: horizontal wall x11
    {definition: "wallcorner", rotate: 270}, // 42: wall corner up/left
    {definition: "gate", rotate: 90, horizSpan: 3}, // 43: vertical gate
    {definition: "wallcap", rotate: 270}, // 44: wall cap up
    {definition: "wall", rotate: 90, repeat: [1, 6]}, // 45: vertical wall x6
    {definition: "mansion", horizSpan: 8, vertSpan: 8}, // 46: mansion
    {definition: "wallcap", rotate: 180}, // 47: wall cap left
    {definition: "wallcap"}, // 48: wall cap right
    {definition: "tree", rotate: 90, vertSpan: 3, horizSpan: 3}, // 49: tree rotated 90
    {definition: "clippedbush1"}, // 50: clipped bush for the cemetery
    {definition: "tombstone"}, // 51: vertical tombstone
    {definition: "philtombstone"}, // 52: Phil Peters tombstone
    {definition: "joanntombstone"}, // 53: Jo Ann Peters tombstone
    {definition: "jasontombstone"}, // 54: Jason Peters tombstone
    {definition: "dorothytombstone"}, // 55: Dorothy Peters tombstone
    {definition: "tree", rotate: 180, vertSpan: 3, horizSpan: 3}, // 56: tree rotated 180
    {definition: "jonathantombstone", rotate: 90}, // 57: Jonathan Gilchrist tombstone
    {definition: "patricktombstone", rotate: 90}, // 58: Patrick Huff tombstone
    {definition: "samanthatombstone", rotate: 90}, // 59: Samantha Huff tombetsone
    {definition: "tombstone", rotate: 90}, // 60: horizontal tombstone
    {definition: "vincenttombstone"}, // 61: Vincent (dad)'s tombstone
    {definition: "martatombstone"}, // 62: Marta (mom)'s tombstone
    {definition: "fredericktombstone"}, // 63: Frederick's tombstone
    {definition: "valerietombstone"}, // 64: Valerie's tombstone
    {definition: "benjamintombstone"}, // 65: Benjamin's tombstone
    {definition: "alberttombstone"}, // 66: Albert's tombstone
    {definition: "donnatombstone"}, // 67: Donna's tombstone
    {definition: "wall", repeat: [14, 1]}, // 68: horizontal wall x14
    {definition: "church", horizSpan: 10, vertSpan: 13}, // 69: church
    {definition: "bench", horizSpan: 2}, // 70: bench facing up
    {definition: "tree", rotate: 270, vertSpan: 3, horizSpan: 3}, // 71: tree rotated 270
    {definition: "bench", rotate: 180, horizSpan: 2}, // 72: bench facing down
    {definition: "swings", horizSpan: 4, vertSpan: 8}, // 73: swings
    {definition: "bench", rotate: 90, horizSpan: 2}, // 74: bench facing right
    {definition: "olderhouse", horizSpan: 7, vertSpan: 6}, // 75: newer house
    {definition: "tenniscourt", horizSpan: 10, vertSpan: 5}, // 76: tennis court
    {definition: "bench", rotate: 270, horizSpan: 2}, // 77: bench facing left
    {definition: "clippedbush2", vertSpan: 2}, // 78: clipped bush for the tennis courts
    {definition: "wallcorner", rotate: 90}, // 79: wall corner down/right
    {definition: "wallcorner", rotate: 180}, // 80: wall corner down/left
    {definition: "wall", rotate: 90, repeat: [1, 7]}, // 81: vertical wall x7
    {definition: "wall", repeat: [5, 1]}, // 82: horizontal wall x5
    {definition: "middleschool", horizSpan: 16, vertSpan: 4}, // 83: middle school
    {definition: "wall", repeat: [7, 1]}, // 84: horizontal wall x7
    {definition: "elementaryschool", horizSpan: 4, vertSpan: 7}, // 85: elementary school
    {definition: "wallcorner"}, // 86: wall corner up/right
    {definition: "highschool", rotate: 270, horizSpan: 16, vertSpan: 4}, // 87: high school
    {definition: "wallcap", rotate: 90}, // 88: wall cap down
    {definition: "wall", rotate: 90, repeat: [1, 5]}, // 89: vertical wall x5
    {definition: "musicstore", vertSpan: 25, horizSpan: 9}, // 90: music store
    {definition: "radiostore", vertSpan: 10, horizSpan: 9}, // 91: radio store
    {definition: "burgerjoint", vertSpan: 15, horizSpan: 9}, // 92: burger joint
    {definition: "woods", vertSpan: 3, horizSpan: 3}, // 93: woods
    {definition: "woods", rotate: 90, vertSpan: 3, horizSpan: 3}, // 94: woods
    {definition: "woods", rotate: 180, vertSpan: 3, horizSpan: 3}, // 95: woods
    {definition: "woods", rotate: 270, vertSpan: 3, horizSpan: 3}, // 96: woods
    {definition: "knittingstore", vertSpan: 15, horizSpan: 7}, // 97: knitting store
    {definition: "coffeeshop", vertSpan: 7, horizSpan: 11}, // 98: coffee shop
    {definition: "toystore", vertSpan: 8, horizSpan: 11}, // 99: toy store
    {definition: "bank", vertSpan: 10, horizSpan: 9}, // 100: bank
    {definition: "realtor", vertSpan: 10, horizSpan: 9}, // 101: realtor
    {definition: "wall", rotate: 90, repeat: [1, 9]}, // 102: vertical wall x9
    {definition: "carlot", vertSpan: 13, horizSpan: 4}, // 103: car lot
    {definition: "car", rotate: 270, vertSpan: 2}, // 104: car facing left
];

var definitions = {
    bank: {type: "bank", image: "bank", leftOffset: 2, rightOffset: 2, topOffset: 2, bottomOffset: 2},
    bench: {type: "bench", image: "bench", leftOffset: 2, rightOffset: 2, topOffset: 7, bottomOffset: 7},
    burgerjoint: {type: "burgerjoint", image: "burgerjoint", leftOffset: 2, rightOffset: 2, topOffset: 2, bottomOffset: 8},
    bush: {type: "bush", image: "bush", leftOffset: 2, rightOffset: 3, topOffset: 1, bottomOffset: 1},
        clippedbush1: {type: "bush", image: "clippedbush1", bottomOffset: 2, leftOffset: 1, rightOffset: 1},
        clippedbush2: {type: "bush", image: "clippedbush2", leftOffset: 1, rightOffset: 1, topOffset: 3},
    car: {type: "car", image: "car", leftOffset: 3, rightOffset: 3, topOffset: 4, bottomOffset: 4},
        yourcar: {type: "yourcar", image: "car", leftOffset: 3, rightOffset: 3, topOffset: 4, bottomOffset: 4},
    carlot: {type: "carlot", image: "carlot", leftOffset: 2, rightOffset: 2, topOffset: 2, bottomOffset: 2},
    church: {type: "church", image: "church", rightOffset: 11, topOffset: 5, bottomOffset: 5},
    coffeeshop: {type: "coffeeshop", image: "coffeeshop", leftOffset: 2, rightOffset: 8, topOffset: 2, bottomOffset: 2},
    elementaryschool: {type: "elementaryschool", image: "elementaryschool", leftOffset: 2, topOffset: 2, bottomOffset: 2},
    fence: {type: "fence", image: "fence", topOffset: 15, bottomOffset: 15},
        fence4way: {type: "fence", image: "fence4way"},
        fencecorner: {type: "fence", image: "fencecorner", bottomOffset: 15, leftOffset: 15},
        fencet: {type: "fence", image: "fencet", bottomOffset: 15},
    forsalesign: {type: "forsalesign", image: "forsalesign", bottomOffset: 36, leftOffset: 32, rightOffset: 22},
    garage: {type: "garage", image: "garage", topOffset: 2, bottomOffset: 2, rightOffset: 10},
    gate: {type: "gate", image: "gate", topOffset: 17, bottomOffset: 17},
    highschool: {type: "highschool", image: "middleschool", leftOffset: 2, rightOffset: 2, topOffset: 2},
    house: {type: "house", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        dufresnehouse: {type: "dufresnehouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        gilchristhouse: {type: "gilchristhouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        huffhouse: {type: "huffhouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        newerhouse: {type: "newerhouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        olderhouse: {type: "olderhouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        petershouse: {type: "petershouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        xinhouse: {type: "xinhouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
        yourhouse: {type: "yourhouse", image: "house", topOffset: 2, bottomOffset: 2, leftOffset: 7},
    knittingstore: {type: "knittingstore", image: "knittingstore", leftOffset: 2, rightOffset: 2, topOffset: 2, bottomOffset: 2},
    mansion: {type: "mansion", image: "mansion", leftOffset: 9, rightOffset: 9, bottomOffset: 9},
    middleschool: {type: "middleschool", image: "middleschool", leftOffset: 2, rightOffset: 2, topOffset: 2},
    musicstore: {type: "musicstore", image: "musicstore", leftOffset: 2, rightOffset: 2, topOffset: 8, bottomOffset: 2},
    pool: {type: "pool", image: "pool", topOffset: 5, bottomOffset: 5, leftOffset: 5, rightOffset: 5},
    radio: {image: "radio", leftOffset: 1, rightOffset: 1, topOffset: 1, bottomOffset: 1},
    radiostore: {type: "radiostore", image: "radiostore", leftOffset: 2, rightOffset: 2, topOffset: 38, bottomOffset: 2},
    realtor: {type: "realtor", image: "realtor", leftOffset: 2, rightOffset: 38, topOffset: 2, bottomOffset: 2},
    swings: {type: "swings", image: "swings", leftOffset: 2, rightOffset: 2, topOffset: 2, bottomOffset: 2},
    tenniscourt: {type: "tenniscourt", image: "tenniscourt", leftOffset: 2, rightOffset: 2, topOffset: 2},
    tombstone: {type: "tombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        alberttombstone: {type: "alberttombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        benjamintombstone: {type: "benjamintombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        donnatombstone: {type: "donnatombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        dorothytombstone: {type: "dorothytombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        fredericktombstone: {type: "fredericktombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        jasontombstone: {type: "jasontombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        joanntombstone: {type: "joanntombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        jonathantombstone: {type: "jonathantombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        martatombstone: {type: "martatombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        patricktombstone: {type: "patricktombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        philtombstone: {type: "philtombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        samanthatombstone: {type: "samanthatombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        valerietombstone: {type: "valerietombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
        vincenttombstone: {type: "vincenttombstone", image: "tombstone", topOffset: 5, bottomOffset: 5, leftOffset: 18, rightOffset: 18},
    toystore: {type: "toystore", image: "toystore", leftOffset: 2, rightOffset: 38, topOffset: 2, bottomOffset: 2},
    tree: {type: "tree", image: "tree", topOffset: 9, bottomOffset: 9, leftOffset: 9, rightOffset: 9},
    wall: {type: "wall", image: "wall", topOffset: 14, bottomOffset: 14},
        wallcorner: {type: "wall", image: "wallcorner", leftOffset: 14, bottomOffset: 14},
        wallcornerwithfence: {type: "wall", image: "wallcornerwithfence", bottomOffset: 14},
        wallcap: {type: "wall", image: "wallcap", topOffset: 14, bottomOffset: 14},
    woods: {type: "woods", image: "tree", topOffset: 9, bottomOffset: 9, leftOffset: 9, rightOffset: 9},
};

// activity
// autobiography
// capitalism
// nature
// isolation
// town
// transportation

var types = {
    alberttombstone: {name: "Albert's Tombstone", lookName: "Albert's tombstone", feelName: "Albert's tombstone", description: "Albert Carlisle {NL} " + (currentYear - 96) + "-" + (currentYear - 18), isolation: 20, town: 10},
    bank: {name: "Bank", lookName: "the bank", feelName: "the bank", description: "This is the oldest building in town. It's had to be refurbished several times over the years. Got a blank stare from the teller when I suggested they rename it to Theseus Savings & Loan.", capitalism: 20, town: 10},
    bench: {name: "Bench", lookName: "a bench", feelName: "the bench", description: "A bunch of these benches have the name of a resident on them, immortality for donating $200 to the town.", activity: 20, autobiography: 10},
    benjamintombstone: {name: "Benjamin's Tombstone", lookName: "Benjamin's tombstone", feelName: "Benjamin's tombstone", description: "Benjamin Carlisle {NL} " + (currentYear - 95) + "-" + (currentYear - 16), isolation: 20, town: 10},
    burgerjoint: {name: "Burger Joint", lookName: "the burger joint", feelName: "the burger joint", description: "Best burgers in town.", capitalism: 20, town: 10},
    bush: {name: "Bush", lookName: "a bush", feelName: "the bush", description: "Everyone grows rhododendrons here. I guess everywhere else too, really.", nature: 20, town: 10},
    car: {name: "Car", lookName: "a car", feelName: "the car", description: "Traffic gets worse here every year, but the population hasn't gotten any bigger. I've been driving more and walking less than I used to be able to, I imagine that's true for a lot of folks.", transportation: 20, town: 10, autobiography: 10},
    carlot: {name: "Car Lot", lookName: "the car lot", feelName: "the car lot", description: "The only car lot in thirty miles. They run a lot of commercials on the local TV stations trying to get folks from out of town to \"drive a little to save a lot.\"", capitalism: 20, transportation: 10},
    church: {name: "Church", lookName: "the church", feelName: "the church", description: "Our Lady of Compassion, founded in 1898. The lights are always on.", town: 20},
    coffeeshop: {name: "Coffee Shop", lookName: "the coffee shop", feelName: "the coffee shop", description: "I've never been a big coffee drinker, but their pastries are all right. Caroline always wants to come here when she visits, she and the owner were classmates.", capitalism: 20, autobiography: 10},
    donnatombstone: {name: "Donna's Tombstone", lookName: "Donna's tombstone", feelName: "Donna's tombstone", description: "Donna Carlisle {NL} " + (currentYear - 92) + "-" + (currentYear - 26), isolation: 20, town: 10},
    dorothytombstone: {name: "Dorothy's Tombstone", lookName: "Dorothy's tombstone", feelName: "Dorothy's tombstone", description: "Dorothy Peters {NL} " + (currentYear - 110) + "-" + (currentYear - 45), isolation: 20, town: 10},
    dufresnehouse: {name: "The Dufresnes' House", lookName: "the Dufresnes' house", feelName: "the Dufresnes' house", description: "The Dufresnes made it big running some kind of business and bought this house three years ago. Vacation renters stay here more often than they do.", town: 20, capitalism: 10},
    elementaryschool: {name: "Elementary School", lookName: "the elementary school", feelName: "the elementary school", description: "Caroline was just old enough to be able to attend both the old and new Whitacre Elementary. She was really attached to the old building and happy to go back for seventh grade.{P}HIDDEN MAP OBJECT! :)", town: 20, autobiography: 10},
    fence: {name: "Fence", lookName: "a fence", feelName: "the fence", description: "They teach that Frost poem in high school, the one that goes, \"Good fences make good neighbors.\" Whitacre's always taken that one to heart.", isolation: 20, town: 10},
    forsalesign: {name: "For Sale Sign", lookName: "a for sale sign", feelName: "the for sale sign", description: "\"For sale: call Janet Schultz.\" Her commercials run on TV all the time. She's visited a few times trying to get me to sell too.", capitalism: 20, town: 10},
    fredericktombstone: {name: "Frederick's Tombstone", lookName: "Frederick's tombstone", feelName: "Frederick's tombstone", description: "Frederick Carlisle {NL} " + (currentYear - 120) + "-" + (currentYear - 41), isolation: 20, town: 10},
    garage: {name: "Garage", lookName: "a garage", feelName: "the garage", description: "A bedroom for the most important member of the family.", transportation: 20},
    gate: {name: "Gate", lookName: "a gate", feelName: "the gate", description: "Probably an automatic car gate. A company selling them set up shop a few years ago and they're still in business, so I guess they're doing all right.", isolation: 20, capitalism: 10, town: 10},
    gilchristhouse: {name: "The Gilchrists' House", lookName: "the Gilchrists' house", feelName: "the Gilchrists' house", description: "Diane Gilchrist and her wife, forget her name, don't leave the house much any more. I saw Diane for the first time in ages at the supermarket last week, she's gotten so thin.", town: 20, isolation: 10},
    highschool: {name: "High School", lookName: "the high school", feelName: "the high school", description: "O Whitacre High, your {NL} Name we revere, your {NL} Halls we most fondly acclaim~", town: 20, autobiography: 10},
    huffhouse: {name: "The Huffs' House", lookName: "the Huffs' house", feelName: "the Huffs' house", description: "Frank Huff has served on the town council for 11 years. It's hard to forget, given how often he reminds everyone about it.", town: 20},
    jasontombstone: {name: "Jason's Tombstone", lookName: "Jason's tombstone", feelName: "Jason's tombstone", description: "Jason Peters {NL} " + (currentYear - 111) + "-" + (currentYear - 32), isolation: 20, town: 10},
    joanntombstone: {name: "Jo Ann's Tombstone", lookName: "Jo Ann's tombstone", feelName: "Jo Ann's tombstone", description: "Jo Ann Peters {NL} " + (currentYear - 76) + "-" + (currentYear - 6), isolation: 20, town: 10},
    jonathantombstone: {name: "Jonathan's Tombstone", lookName: "Jonathan's tombstone", feelName: "Jonathan's tombstone", description: "Jonathan Gilchrist {NL} " + (currentYear - 25) + "-" + (currentYear - 1), isolation: 20, town: 10},
    knittingstore: {name: "Knitting Store", lookName: "the knitting store", feelName: "the knitting store", description: "It's called Knitacre and I hate it.", capitalism: 20, activity: 10},
    mansion: {name: "Mansion", lookName: "a mansion", feelName: "the mansion", description: "I think they call these \"McMansions\" on the internet.{P}HIDDEN MAP OBJECT! :)", capitalism: 20, town: 10},
    martatombstone: {name: "Marta's Tombstone", lookName: "Marta's tombstone", feelName: "Marta's tombstone", description: "Marta " + lastName + " {NL} " + (currentYear - 103) + "-" + (currentYear - 32), isolation: 20, autobiography: 10},
    middleschool: {name: "Middle School", lookName: "the middle school", feelName: "the middle school", description: "This used to be Whitacre Elementary & Middle School, but our population grew enough in the '80s that they moved the elementary school to a new building across the street.", town: 20},
    musicstore: {name: "Music Store", lookName: "the music store", feelName: "the music store", description: "The only music store in town, been here since I was a kid. Pianos cost more than you think.", capitalism: 20, activity: 10},
    newerhouse: {name: "Newer House", lookName: "a newer house", feelName: "the newer house", description: "One of the new houses they've built around here. Lots of money flowing into the construction market.", town: 20, capitalism: 10},
    olderhouse: {name: "Older House", lookName: "an older house", feelName: "the older house", description: "One of older houses around here. Not old enough to be historically significant, just one that's been here for a few generations.", town: 20},
    patricktombstone: {name: "Patrick's Tombstone", lookName: "Patrick's tombstone", feelName: "Patrick's tombstone", description: "Patrick Huff {NL} " + (currentYear - 93) + "-" + (currentYear - 21), isolation: 20, town: 10},
    petershouse: {name: "The Peters' House", lookName: "the Peters' house", feelName: "the Peters' house", description: "The furthest back I can remember, this was always the Peterses house... until Phil died last year and his kids put it on the market. They never liked it here.", town: 20},
    philtombstone: {name: "Phil's Tombstone", lookName: "Phil's tombstone", feelName: "Phil's tombstone", description: "Phil Peters {NL} " + (currentYear - 78) + "-" + (currentYear - 1), isolation: 20, town: 10},
    pool: {name: "Pool", lookName: "a pool", feelName: "the pool", description: "I never learned how to swim. Took Caroline to the community pool all the time when she was growing up, though.", activity: 20, autobiography: 10},
    radiostore: {name: "Radio Store", lookName: "the radio store", feelName: "the radio store", description: "This has been Carlisle Computers for 20 years now, but I've never been able to stop calling it Carlisle Radio.", capitalism: 20, autobiography: 10},
    realtor: {name: "Realtor", lookName: "the realtor", feelName: "the realtor", description: "Schultz & Schultz Realty, conveniently located next door to the money source you'll need to visit to pay the ridiculous real estate prices.", capitalism: 20, town: 10},
    samanthatombstone: {name: "Samantha's Tombstone", lookName: "Samantha's tombstone", feelName: "Samantha's tombstone", description: "Samantha Huff {NL} " + (currentYear - 96) + "-" + (currentYear - 11), isolation: 20, town: 10},
    swings: {name: "Swings", lookName: "some swings", feelName: "the swings", description: "The old swing set was knocked over during a storm five years ago, and they replaced it with a new one that looks exactly the same.", activity: 20},
    tenniscourt: {name: "Tennis Court", lookName: "a tennis court", feelName: "the tennis court", description: "Tennis is pretty popular here, at least one of these courts is almost always being used.", activity: 20},
    tombstone: {name: "Tombstone", lookName: "a tombstone", feelName: "the tombstone", description: "Rest in peace.", isolation: 20},
    toystore: {name: "Toy Store", lookName: "the toy store", feelName: "the toy store", description: "The Huffs still make toys out of wood from the forest outside town, but obviously the electronic ones are way more popular these days.", capitalism: 20, town: 10},
    tree: {name: "Tree", lookName: "a tree", feelName: "the tree", description: "We're already surrounded by trees. Never saw the point of bringing them in here too.", nature: 20},
    valerietombstone: {name: "Valerie's Tombstone", lookName: "Valerie's tombstone", feelName: "Valerie's tombstone", description: "Valerie Carlisle {NL} " + (currentYear - 119) + "-" + (currentYear - 41), isolation: 20, town: 10},
    vincenttombstone: {name: "Vincent's Tombstone", lookName: "Vincent's tombstone", feelName: "Vincent's tombstone", description: "Vincent " + lastName + " {NL} " + (currentYear - 102) + "-" + (currentYear - 43), isolation: 20, autobiography: 10},
    wall: {name: "Wall", lookName: "a wall", feelName: "the wall", description: "There used to be a quarry east of town, and it was a point of pride that many of our walls and buildings were built from stone from it. Some people even still have Whitacre Quarry signs up.", isolation: 20, town: 10},
    woods: {name: "Woods", lookName: "the woods", feelName: "the woods", description: "The outskirts of the woods start here. The actual campgrounds and such are further north.", nature: 20, activity: 10},
    xinhouse: {name: "The Xins' House", lookName: "the Xins' house", feelName: "the Xins' house", description: "Wayland Xin installed a pool in his backyard last month along with some new lighting, and they shine right into my bedroom.", town: 20, autobiography: 10},
    yourcar: {name: "Your Car", lookName: "your car", feelName: "your car", description: "I bought this around ten years ago, after ol' Fanny finally breathed her last. We tried to bury her in the family plot, but the cemetery wouldn't allow it.", autobiography: 20, transportation: 10},
    yourhouse: {name: "Your House", lookName: "your house", feelName: "your house", description: "My parents bought this house while mom was pregnant. Dad used to tell me he built it, but it looks the same as every other house here.", autobiography: 20},
};
