// endingscene.js
// Controller for the ending scene.
function EndingSceneController() {
    this.makeCurrentPage = function() {
        var pageText = this.pageGenerators[this.currentPage]();
        this.currentPageTexts = splitText(pageText + " " + lastActionKeyPressed, endingTextStartX, endingTextStartY, endingTextWidth);

    }
    
    this.page1Generator = function() {
        var text = "Police searching for missing author {NL} By Camilla Suarez, csuarez@whitacrestar.com {NL} {NL} ";
        text += "The Whitacre Police Department is asking for the public's help in locating a missing person. {NL} {NL} ";
        text += firstName + " " + lastName + " was last seen at the J&K Foods on Rockton Street about 3 p.m. Thursday, police said. " + lastName + " is 70 years old, 5 feet 11 inches tall, and weighs about 170 pounds. {NL} {NL} ";
        text += lastName + " was reported missing by their daughter, Caroline, on Sunday.";
        
        return text;
    }
    
    this.page2Generator = function() {
        // Calculate how many points each characteristic got, as well as the
        // combined chaos level.
        var characteristics = ["activity", "autobiography", "capitalism", "nature", "isolation", "town", "transportation"];
        var scores = new Object();
        for (var i = 0; i < characteristics.length; i++) {
            scores[characteristics[i]] = 0;
        }
        var totalItems = 0;
        var totalChaos = 0;
        for (var i = 0; i < scenes[1].inventory.length; i++) {
            if (scenes[1].inventory[i]) {
                totalItems += 1;
                totalChaos += scenes[1].inventory[i].chaos;
                var type = types[definitions[scenes[1].inventory[i].object.definition].type];
                for (var j = 0; j < characteristics.length; j++) {
                    scores[characteristics[j]] += type[characteristics[j]] || 0;
                }
            }
        }
        var averageChaos = totalChaos / totalItems;
        
        // Find the strongest characteristics covered in the inventory. If just
        // one item was picked, then that item's main characteristic will be
        // chosen by default, even though its score will be too low on its own.
        var strongCharacteristics = [];
        for (var i = 0; i < characteristics.length; i++) {
            if (scores[characteristics[i]] >= 40 || (scores[characteristics[i]] == 20 && totalItems == 1)) {
                strongCharacteristics.push(characteristics[i]);
            }
        }
        
        // Choose the title and description of the manuscript based on the
        // number and type of strong characteristics.
        var manuscriptTitle = "", manuscriptDescription = "";
        if (strongCharacteristics.length == 0) {
            manuscriptTitle = "Thoughts and Reflections";
            manuscriptDescription = " and is a collection of essays on various subjects.";
        } else if (strongCharacteristics.length == 1) {
            if (strongCharacteristics[0] == "activity") {
                manuscriptTitle = "Do";
                manuscriptDescription = " and is a collection of writings on the ways people occupy themselves.";
            } else if (strongCharacteristics[0] == "autobiography") {
                manuscriptTitle = "My Life";
                manuscriptDescription = " and is a free-flowing collection of autobiographical stories.";
            } else if (strongCharacteristics[0] == "capitalism") {
                manuscriptTitle = "The Cult of the Dollar";
                manuscriptDescription = " and is a collection of writings on capitalism.";
            } else if (strongCharacteristics[0] == "nature") {
                manuscriptTitle = "Gaia";
                manuscriptDescription = " and is a collection of writings on plants and the natural world.";
            } else if (strongCharacteristics[0] == "isolation") {
                manuscriptTitle = "Oneself";
                manuscriptDescription = " and is a collection of writings on the ways we isolate ourselves.";
            } else if (strongCharacteristics[0] == "town") {
                manuscriptTitle = "A Town Called Whitacre";
                manuscriptDescription = " and is a collection of stories about the people in their hometown.";
            } else if (strongCharacteristics[0] == "transportation") {
                manuscriptTitle = "Automobility";
                manuscriptDescription = " and is a collection of writings about cars, a recurring subject in their works.";
            }
        } else if (strongCharacteristics.length == 2) {
            if (strongCharacteristics.includes("activity")) {
                if (strongCharacteristics.includes("autobiography")) {
                    manuscriptTitle = "Punkass Kid";
                    manuscriptDescription = " and is a collection of stories of trouble they got into growing up.";
                } else if (strongCharacteristics.includes("capitalism")) {
                    manuscriptTitle = "The Business of Leisure";
                    manuscriptDescription = " and is a collection of writings on the sports and recreation industries.";
                } else if (strongCharacteristics.includes("nature")) {
                    manuscriptTitle = "Outdoors";
                    manuscriptDescription = " and is an exhortation of wilderness activities.";
                } else if (strongCharacteristics.includes("isolation")) {
                    manuscriptTitle = "Diversions";
                    manuscriptDescription = " and is a collection of writings on losing oneself in activities, intentionally to avoid or cope with things.";
                } else if (strongCharacteristics.includes("town")) {
                    manuscriptTitle = "Busybodies";
                    manuscriptDescription = " and is a collection of stories about Whitacre sports and recreation.";
                } else if (strongCharacteristics.includes("transportation")) {
                    manuscriptTitle = "On the Road";
                    manuscriptDescription = " and is a collection of stories about exploring by car.";
                }
            } else if (strongCharacteristics.includes("autobiography")) {
                if (strongCharacteristics.includes("capitalism")) {
                    manuscriptTitle = "Don't Do It";
                    manuscriptDescription = " and is a collection of business advice for young authors, based on their own experience working with publishers.";
                } else if (strongCharacteristics.includes("nature")) {
                    manuscriptTitle = "Burn Them All Down for All I Care";
                    manuscriptDescription = " and is a collection of writings about their often contentious relationship with plants.";
                } else if (strongCharacteristics.includes("isolation")) {
                    manuscriptTitle = "Atropos";
                    manuscriptDescription = " and is a collection of personal stories about losing and drifting away from friends and family.";
                } else if (strongCharacteristics.includes("town")) {
                    manuscriptTitle = "My Whitacre";
                    manuscriptDescription = " and is a collection of personal stories about their hometown.";
                } else if (strongCharacteristics.includes("transportation")) {
                    manuscriptTitle = "Autobiography";
                    manuscriptDescription = " and is a collection of stories centered around a Chevrolet Bel Air they used to own, affectionally named Fanny.";
                }
            } else if (strongCharacteristics.includes("capitalism")) {
                if (strongCharacteristics.includes("nature")) {
                    manuscriptTitle = "The Green Market";
                    manuscriptDescription = " and is a collection of writings about the business of buying, growing, and maintaining the plants of Whitacre.";
                } else if (strongCharacteristics.includes("isolation")) {
                    manuscriptTitle = "Can't Buy Me Love";
                    manuscriptDescription = " and is a collection of writings about money and strained relationships.";
                } else if (strongCharacteristics.includes("town")) {
                    manuscriptTitle = "Mammon";
                    manuscriptDescription = " and is a collection of writings about businesses in their hometown, small and large.";
                } else if (strongCharacteristics.includes("transportation")) {
                    manuscriptTitle = "Rials on Wheels";
                    manuscriptDescription = " and is a collection of writings about the automobile industry.";
                }
            } else if (strongCharacteristics.includes("nature")) {
                if (strongCharacteristics.includes("isolation")) {
                    manuscriptTitle = "Hedges";
                    manuscriptDescription = " and is a collection of stories about the ways geography and nature isolate people.";
                } else if (strongCharacteristics.includes("town")) {
                    manuscriptTitle = "Rhododendrons";
                    manuscriptDescription = " and is a collection of stories about their hometown, centered around Whitacre's omnipresent rhododendron bushes.";
                } else if (strongCharacteristics.includes("transportation")) {
                    manuscriptTitle = "Reclamation";
                    manuscriptDescription = " and is a collection of writings on conservation, centered on an overgrown car abandoned in the woods they found as a child and visited frequently.";
                }
            } else if (strongCharacteristics.includes("isolation")) {
                if (strongCharacteristics.includes("town")) {
                    manuscriptTitle = "Lockbox";
                    manuscriptDescription = " and is a collection of stories about Whitacre's residents and their close or not-close relationships.";
                } else if (strongCharacteristics.includes("transportation")) {
                    manuscriptTitle = "Man's Best Friend";
                    manuscriptDescription = " and is a collection of writings looking at the ways cars have isolated people from one another.";
                }
            } else if (strongCharacteristics.includes("town")) {
                // & transportation
                manuscriptTitle = "Carrus City";
                manuscriptDescription = " and is a collection of car-related anecdotes from their hometown.";
            }
        } else {
            manuscriptTitle = "Loom";
            manuscriptDescription = " and is a dense work, weaving together a wide range of topics inspired by their hometown.";
        }
        if (totalItems == 0) {
            manuscriptTitle = "";
        }
        if (averageChaos >= 0.5) {
            manuscriptDescription = " but that the entire text is indecipherable scrawl.";
        }
        
        // Chaos up the manuscript's title.
        if (manuscriptTitle) {
            var words = manuscriptTitle.split(" ");
            var chaosedTitle = "";
            for (var i = 0; i < words.length; i++) {
                words[i] = "{" + averageChaos + "}" + words[i];
            }
            manuscriptTitle = words.join(" ");
        }
        
        var text = lastName + " is best known as an author of nonfiction books and essays. Their last work was published in " + (currentYear - 13) + ". {NL} {NL} ";
        if (!manuscriptTitle) {
            text += "After the disappearance of " + lastName + " was announced by police yesterday, publisher Wingfield Books said they received a package from them last week. Wingfield spokesman Don Kamau said they originally believed it was a new manuscript, but after opening it they found every page was completely blank.";
        } else {
            text += "After the disappearance of " + lastName + " was announced by police yesterday, publisher Wingfield Books said they received a manuscript from them last week. Wingfield spokesman Don Kamau said it's titled " + manuscriptTitle + manuscriptDescription;
        }
        return text;
    }
    
    this.page3Generator = function() {
        var text = "Police have searched the " + lastName + " residence but said there was no indication of where they may have gone and no signs of a struggle or robbery. The only thing known to be missing was an old radio, ";
        if (scenes[1].radio) {
            text += "which was recovered elsewhere in Whitacre. Police cannot say at this time whether the radio was taken there by " + lastName + " or someone else. {NL} {NL} ";
        } else {
            text += "which has yet to be found. {NL} {NL} ";
        }
        text += "Anyone with information on the whereabouts of " + lastName + " is asked to call the Whitacre Police Department at 911 or 397-141-2300."
        
        return text;
    }
    
    this.page4Generator = function() {
        return "THE END";
    }
    
    this.pageGenerators = [this.page1Generator, this.page2Generator, this.page3Generator, this.page4Generator];
    
    this.chaos = 0;
    this.currentPage = 0;
    this.currentPageTexts = [];
}

EndingSceneController.prototype.draw = function(timestamp) {
    // If we're not transitioning, handle player input.
    if (!this.transitioning) {        
        // Update the chaos.
        var xVelocity = 0, yVelocity = 0;
        if (upKeyDown()) {
            // up
            yVelocity -= 1;
        }
        if (downKeyDown()) {
            // down
            yVelocity += 1;
        }
        if (leftKeyDown()) {
            // left
            xVelocity -= 1;
        } 
        if (rightKeyDown()) {
            // right
            xVelocity += 1;
        }
        if (xVelocity != 0 && yVelocity != 0) {
            xVelocity /= 2;
            yVelocity /= 2;
        }
        this.chaos += (xVelocity + yVelocity) * openingAndEndingChaosStep;
    
        if (this.chaos < openingAndEndingChaosStep) {
            this.chaos = openingAndEndingChaosStep;
        } else if (this.chaos > 1) {
            this.chaos = 1;
        }
        setMusicChaos(this.chaos);
    }

    drawTexts(this.currentPageTexts, gameCanvasContext, endingTextStartX, endingTextStartY, this.chaos);
}

EndingSceneController.prototype.startTransitionIn = function() {
    this.transitioning = true;
    this.chaos = openingAndEndingChaosStep;
    this.currentPage = 0;
    this.currentPageTexts = [];
    this.makeCurrentPage();
}

EndingSceneController.prototype.finishTransitionIn = function() {
    this.transitioning = false;
}

EndingSceneController.prototype.startTransitionOut = function() {
    this.transitioning = true;
}

EndingSceneController.prototype.finishTransitionOut = function() {
    
}

EndingSceneController.prototype.triggerAction = function() {
    if (this.transitioning) {
        return;
    }
    
    if (this.currentPage == this.pageGenerators.length - 1) {
        startTransition(0);
    } else {
        this.currentPage += 1;
        this.makeCurrentPage();
    }
}
