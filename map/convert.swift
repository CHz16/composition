#!/usr/bin/env xcrun swift

import Cocoa

let inputUrl = URL(fileURLWithPath: "map.txt")
let mapData = try String(contentsOf: inputUrl, encoding: String.Encoding.utf8)

let outputUrl = URL(fileURLWithPath: "../composition/map.js")
func write(_ s: String) {
    try? (s + "\n").write(to: outputUrl, atomically: false, encoding: String.Encoding.utf8)
}


var output = "// map.js\n"
output += "// Map data.\n"
output += "// Automatically generated with the convert.swift script in the root map/ directory.\n\n"
output += "var map = [\n"

var lines: [String] = []
mapData.enumerateLines { (line, stop) in
    lines.append(line.components(separatedBy: "\t").map { Int($0) ?? 0 }.description)
}
output += "    " + lines.joined(separator: ",\n    ")

output += "\n];\n"


try? output.write(to: outputUrl, atomically: false, encoding: String.Encoding.utf8)
